class Student:
    def __init__(self, name, roll_no):
        self.name = name
        self.roll_no = roll_no

    def __repr__(self):
        return self.name

class Score:
    def __init__(self, student, subject, score):
        self.student = student
        self.subject = subject
        self.score = score
    
    def __repr__(self):
        return f'The student {self.student} has a score {self.score} in subject {self.subject}'
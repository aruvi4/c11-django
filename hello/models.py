from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Student (models.Model):
    name = models.CharField(max_length=25)
    roll_no = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'Student: {self.roll_no} : {self.name}'

class Score (models.Model):
    subject_name = models.CharField(max_length=25)
    subject_score = models.IntegerField()
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.subject_name} - {self.subject_score}'

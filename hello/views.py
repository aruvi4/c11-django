from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.http import HttpResponse, Http404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from .models import Student, Score
from django.contrib.auth.models import User

# Create your views here.

#localhost:8000/hello/fibonacci?n=10 should print the 10th fibonacci number

def say_hello(request):
    default_name = 'user'
    #request.GET contains all the GET request query params
    #give query params in the format:
    #url?key1=value1&key2=value2&...
    name_ = request.GET.get('name', default_name)
    context = {'greeting': "Hi", 'name': name_}
    #context = {}
    return render(request, 'hello/greetings.html', context)

def say_goodbye(request):
    context = {'greeting': "Goodbye!"}
    return render(request, 'hello/greetings.html', context)

def nth_fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return nth_fibonacci(n - 1) + nth_fibonacci(n - 2)

def get_nth_fibonacci(request):
    n = int(request.GET.get('n', 0))
    fibonacci = nth_fibonacci(n)
    context = {'n': n,'fibonacci':fibonacci}
    return render(request, 'hello/fibonacci.html', context)

def get_nth_fibonacci_alt(request, n):
    fibonacci = nth_fibonacci(n)
    context = {'n': n,'fibonacci':fibonacci}
    return render(request, 'hello/fibonacci.html', context)

def nth_fibonacci_post(request):
    if request.method == 'GET':
        return render(request, 'hello/fibonacci.html')
    if request.method == 'POST':
        n = int(request.POST.get('num', 0))
        fibonacci = [nth_fibonacci(i) for i in range(n + 1)]
        context = {'n': n, 'fibonacci': fibonacci}
        return render(request, 'hello/fibonacci.html', context)

@permission_required('hello.view_student', login_url='login')
def student_list(request):
    students = get_list_or_404(Student)
    context = {'students': students}
    return render(request, 'hello/student_list.html', context)

@permission_required('hello.view_student', login_url='login')
def student_detail(request, roll_no_):
    student = get_object_or_404(Student, roll_no=roll_no_)
    if (request.user != student.user):
        return redirect('login')
    scores = get_list_or_404(Score, student=student)
    context = {'student': student, 'scores': scores}
    #context['user_has_permission'] = student.user.username == request.session.get("logged_in_user")
    
    return render(request, 'hello/student_detail.html', context)

def update_or_create_student(request, id_=None):
    if request.method == "GET":
        context = {}
        if id_ is not None:
            s = get_object_or_404(Student, pk=id_)
            context['student'] = s
        return render(request, 'hello/admission_form.html', context)
    if request.method == "POST":
        name_ = request.POST.get('name', '')
        roll_no_ = int(request.POST.get('roll', '0'))
        id = int(request.POST.get('id', '0'))
        if id != 0:
            s = get_object_or_404(Student, id=id)
            s.name = name_
            s.roll_no = roll_no_
        else:
            s = Student.objects.create(roll_no = roll_no_, name=name_)
        s.save()
        return redirect("student_list")

def delete_student(request, roll_no_):
    s = get_object_or_404(Student, roll_no=roll_no_)
    s.delete()
    return redirect("student_list")

def login_view(request):
    error_message = ''
    context = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
        else:
            error_message = 'Wrong password'
        context['error_message'] = error_message
    return render(request, 'hello/login.html', context)

def logout_view(request):
    logout(request)
    return redirect('login')